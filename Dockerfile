FROM tolproject/tol-prod-xenial:3.4

ADD https://s3.amazonaws.com/rstudio-server/current.ver /tmp/ver

ENV CRAN_URL https://cloud.r-project.org/

RUN apt-get -y update \
  && apt-get -y install --no-install-recommends --no-install-suggests \
     curl gdebi-core git libapparmor1 libedit2 lsb-release psmisc sudo

RUN curl -S -o /tmp/rstudio.deb \
      https://download2.rstudio.org/server/trusty/amd64/rstudio-server-1.2.1335-amd64.deb \
  && gdebi -n /tmp/rstudio.deb \
  && rm -rf /tmp/rstudio.deb /tmp/ver \
  && echo "server-app-armor-enabled=0" >> /etc/rstudio/rserver.conf \
  && apt-get -y autoremove \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

RUN useradd -m -d /home/rstudio -g staff rstudio \
  && groupadd -g 10000 data-share \
  && usermod -a -G data-share rstudio \
  && echo "rstudio:rstudio" | chpasswd \
  && echo "r-cran-repos=${CRAN_URL}" >> /etc/rstudio/rsession.conf

EXPOSE 8787

COPY docker-entrypoint.sh /

ENTRYPOINT [ "/docker-entrypoint.sh" ]
